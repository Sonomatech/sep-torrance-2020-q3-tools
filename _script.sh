#!/usr/bin/bash

echo "INSERT INTO \`Measurement\` (\`DataStreamID\`,\`DataID\`, \`Value\`, \`QCCode\`, \`OPCode\`)\nVALUES" > 4776_update.sql

cat 4776_data_changes_full.csv | awk -F "," '{
	print "(" $1 "," $2 "," $6 "," $7 "," $8 ")"
}' | sed '$!s/$/,/' >> 4776_update.sql

echo "ON DUPLICATE KEY UPDATE 
\`Value\` = VALUES(\`Value\`),
\`QCCode\` = VALUES(\`QCCode\`),
\`OPCode\` = VALUES(\`OPCode\`);\n\n" >> 4776_update.sql

echo "INSERT INTO \`QCLogData\` (\`DataID\`, \`QCLogID\`, \`OldValue\`, \`NewValue\`, \`OldOPCode\`, \`NewOPCode\`, \`OldQCCode\`, \`NewQCCode\`)\nVALUES\n" >> 4776_qc.sql

cat 4776_data_changes_full.csv | awk -F "," '{
	print "(" $2 ",464084736," $3 "," $6 "," $4 "," $7 "," $5 "," $8 ")"
}' | sed '$!s/$/,/' >> 4776_qc.sql

echo ";\n\n" >> 4776_qc.sql