#!/usr/bin/perl

use strict;
use warnings;

my ($input_data_stream_id) = @ARGV;

open(CHANGES_FILE, '<', $input_data_stream_id."_changes.csv") or die $!;

open(UPDATE_FILE, '>', $input_data_stream_id."_update.sql") or die $!;
open(QC_FILE, '>', $input_data_stream_id."_qc.sql") or die $!;


print UPDATE_FILE "INSERT INTO \`Measurement\` (\`DataStreamID\`,\`DataID\`, \`Value\`, \`QCCode\`, \`OPCode\`)\nVALUES";
print QC_FILE "INSERT INTO \`QCLogData\` (\`DataID\`, \`QCLogID\`, \`OldValue\`, \`NewValue\`, \`OldOPCode\`, \`NewOPCode\`, \`OldQCCode\`, \`NewQCCode\`)\nVALUES\n";

my $last;
while(<CHANGES_FILE>){
   $last = $_ if eof;
   $_ =~ s/^\s+|\s+$//g;
   my ($data_stream_id, $data_id, $old_value, $old_qc_code, $old_op_code, $new_value, $new_qc_code, $new_op_code)
   	= split(/,/, $_);

   	if(
		 !defined $data_stream_id ||
		 !defined $data_id ||
		 !defined $old_value ||
		 !defined $old_qc_code ||
		 !defined $old_op_code ||
		 !defined $new_value ||
		 !defined $new_qc_code ||
		 !defined $new_op_code
	) {
   		print "\n$input_data_stream_id INVALID LINE $. : $_\n";
   		next;
   	}

   	print UPDATE_FILE "($data_stream_id, $data_id, $new_value, $new_qc_code, $new_op_code)";
   	print QC_FILE "($data_id, 464084736, $old_value, $new_value, $old_op_code, $new_op_code, $old_qc_code, $new_qc_code)";
   	if(!$last) {
   		print UPDATE_FILE ",";
   		print QC_FILE ",";
   	} else {
   		print QC_FILE ";";
   	}
   	print UPDATE_FILE "\n";
   	print QC_FILE "\n";
}


print UPDATE_FILE "ON DUPLICATE KEY UPDATE 
\`Value\` = VALUES(\`Value\`),
\`QCCode\` = VALUES(\`QCCode\`),
\`OPCode\` = VALUES(\`OPCode\`);\n\n";

close CHANGES_FILE;
close UPDATE_FILE;
close QC_FILE;